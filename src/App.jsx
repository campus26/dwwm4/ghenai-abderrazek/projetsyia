import './App.scss'
import { Map } from './assets/Components/Map';
import { Form } from './assets/Components/Form';
import { useCallback, useState } from 'react';
import Sidebar from './assets/Components/Sidebar';

function App() {
  // States
  const [encodedPolyline,setEncodedPolyline] = useState("");
  const [routeInfo,setRouteInfo] = useState("");
  const [detailsRoute,setDetailsRoute] = useState([]);
  const [cityWaypoints,setCityWaypoints] = useState([]);

  // Callbacks
  const setPolyline = useCallback(data => {
    setEncodedPolyline(data);
  })
  const setRoute = useCallback(data => {
    setRouteInfo(data);
  })
  const setDetails = useCallback(data => {
    setDetailsRoute(data);
  });
  const setCityPoints = useCallback(data => {
    setCityWaypoints(data);
  });

  return (
    <>
      <Form setPolyline={setPolyline} setRouteInfo={setRoute} setDetails={setDetails} setCityWaypoints={setCityPoints} />
      <Map encodedPolyline={encodedPolyline} routeInfo={routeInfo} cityWaypoints={cityWaypoints} />
      <Sidebar detailsRoute={detailsRoute} />
    </>
  )
}

export default App
