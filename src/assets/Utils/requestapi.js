export async function getDataForAIGeneratedRoute(nbDays,start,end,theme) {
  const url = `http://localhost:8000/openai`;
  const res = await fetch(url,{
      headers:{
          'Content-Type':'application/json; charset=utf-8'
      },
      method:"POST",
      body: JSON.stringify({
          "nbDays":nbDays,
          "start":start,
          "end":end,
          "theme":theme
      })
  });
  const data = await res.json();
  return data;
}

export async function getDirectionForRoute(positions) {
  const url = `http://localhost:8000/ors`;
  const res = await fetch(url,{
          headers:{
              'Content-Type':'application/json; charset=utf-8'
          },
          method:"POST",
          body: JSON.stringify({
              "coordinates":positions
          })
          }
      );
  const data = await res.json();
  return(data);
}