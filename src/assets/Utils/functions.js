export function secToHourMin(seconds) {
    const hours = Math.floor(seconds / 3600)
    const minutes = Math.floor((seconds % 3600) / 60)
  
    if (hours > 0) {
      return `${hours} h ${minutes} min`
    } else {
      return `${minutes} min`
    }
}