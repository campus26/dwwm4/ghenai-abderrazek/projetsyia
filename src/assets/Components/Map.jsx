import polyline from "@mapbox/polyline";
import { React, useRef } from "react";
import { MapContainer, TileLayer, ZoomControl } from 'react-leaflet';

export function Map({encodedPolyline, routeInfo, cityWaypoints}) {
    const mapref = useRef(null);
    const map = mapref.current;
    if (map != null) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
    }
    if ( encodedPolyline != "" && map != null){
        const decodedPolyline = polyline.decode(encodedPolyline);
        const direction = L.polyline(decodedPolyline, {weight:3, color: '#0066ff'}).addTo(map);
        // Création des marqueurs
        if (cityWaypoints){
            cityWaypoints.forEach(nb => {
                L.marker(decodedPolyline[nb],{icon: L.icon({iconUrl: 'src/assets/Icons/circle-dot-solid.svg',iconSize:[25,25]})}).addTo(map);
            });
        }
        direction.bindPopup(routeInfo);
        direction.on('mouseover', function(e) {
            this.mySavedWeight = this.options.weight;
            this.openPopup(e.latlng);
            this.setStyle({
            weight: 5
            });
        });
        direction.on('mouseout', function() {
            this.closePopup();
            this.setStyle({
                weight: this.mySavedWeight
            });
        });
        map.flyTo(direction._latlngs[0],11);
    }
    return (
        <>
            <MapContainer center={[42.398648,-83.164558]} zoom={8} zoomControl={false} scrollWheelZoom={true} ref={mapref}>
                <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' />
                <ZoomControl position="bottomright" />
            </MapContainer>
        </>
    );
}