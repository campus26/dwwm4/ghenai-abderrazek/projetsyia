import Card from "./Card";
import { React, useState } from "react";

export default function Sidebar({ detailsRoute }) {
  // Supposons que vous ayez une liste de données pour chaque carte
  const [sidebarVisible, setSidebarVisible] = useState(false);

  const toggleSidebar = () => {
    setSidebarVisible(!sidebarVisible);
  };

  const listCard = detailsRoute.map((activity, index) => {
    return (
      <Card
        key={index}
        city={activity[0]}
        name={activity[1]}
        description={activity[2]}
        duration={activity[3]}
      />
    );
  });

  return (
    <div className={`sidebar ${sidebarVisible ? "" : "hidden"}`}>
      <h2>Détail de l'itinéraire</h2>
      <ul className="sidebar-menu">{listCard}</ul>
      <input
        onClick={toggleSidebar}
        type="image"
        src="src/assets/Icons/chevron-left-solid.svg"
      />
    </div>
  );
}
