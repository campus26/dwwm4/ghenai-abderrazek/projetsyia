export default function Card({ city, description, name, duration }) {
  return (
    <div className="card">
      <div className="title">
        <img src="src/assets/Icons/circle-dot-solid.svg" />
        <h4 className="card-title">{city}</h4>
      </div>
      <div className="card-details">
        <h6>{name}</h6>
        <p className="description">{description}</p>
        <p>{duration}</p>
      </div>
    </div>
  );
}
