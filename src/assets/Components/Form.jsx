import { React, useEffect, useState } from "react";
import { getDataForAIGeneratedRoute, getDirectionForRoute } from "../Utils/requestapi";
import { secToHourMin } from "../Utils/functions";

export function Form({setPolyline, setRouteInfo, setDetails, setCityWaypoints}) {
    const [startAdress, setStartAdress] = useState("");
    const [endAdress, setEndAdress] = useState("");
    const [days,setDays] = useState("");
    const [theme,setTheme] = useState("");

    function handleStartChange(e) {
        setStartAdress(e.target.value);
    }

    function handleEndChange(e) {
        setEndAdress(e.target.value);
    }

    function handleDaysChange(e) {
        setDays(e.target.value);
    }

    function handleThemeChange(e) {
        setTheme(e.target.value);
    }

    async function handleSubmit(e) {
        e.preventDefault();
        // ChatGPT Request
        const aiGeneratedRoute = await getDataForAIGeneratedRoute(days,startAdress,endAdress,theme);
        const detailsRoute = [];
        const positions = [];
        aiGeneratedRoute.forEach(city => {
            positions.push([city.geolocalisation.longitude,city.geolocalisation.latitude]);
            city.activities.forEach(activity => {
                detailsRoute.push([city.city,activity.name,activity.description,activity.duration]);
            });
        });
        setDetails(detailsRoute);
        // Polyline
        const routeInfo = await getDirectionForRoute(positions);
        setCityWaypoints(routeInfo.way_points);
        setPolyline(routeInfo.geometry);
        setRouteInfo(`Distance : ${Math.round(routeInfo.summary.distance)} km<br/>Temps :  ${secToHourMin(routeInfo.summary.duration)}`);
    }

    return (
        <form action="" id="localise" onSubmit={handleSubmit}>
            <div>
                <label htmlFor="start">Départ <span>*</span></label>
                <input id="start" type="text" placeholder="Ville de départ" value={startAdress} onChange={handleStartChange} />
            </div>
            <div>
                <label htmlFor="end">Arrivée <span>*</span></label>
                <input id="end" type="text" placeholder="Ville d'arrivée" value={endAdress} onChange={handleEndChange} />
            </div>
            <div>
                <label htmlFor="days">Nombres de jours <span>*</span></label>
                <input id="days" type="text" placeholder="Nombre de jours" value={days} onChange={handleDaysChange} />
            </div>
            <div>
                <label htmlFor="theme">Thème</label>
                <input id="theme" type="text" placeholder="Sportif, culturel, touristique" value={theme} onChange={handleThemeChange} />
            </div>
            <input type="image" src="src/assets/Icons/search-icon.svg"/>
        </form>
  );
}